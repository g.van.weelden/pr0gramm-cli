package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/guus-vanweelden/pr0gramm-cli/item"
)

var itemId = flag.Int("item-id", 0, "the ID of the Item")
var userName = flag.String("user", "", "your username")
var passWord = flag.String("pass", "", "your password")

func main() {
	flag.Parse()

	log.Printf("Called id: %d", *itemId)
	urlObj := item.GenerateLink(*itemId)
	log.Printf("url: %+v", urlObj)

	resp, err := http.Get(urlObj.String())
	if err != nil {
		log.Printf("Error getting the content: %s", err)
		return
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading the content: %s", err)
		return
	}
	// log.Printf("Content:\n%s", string(content))
	itemInfo := new(item.Info)
	err = json.Unmarshal(content, itemInfo)
	if err != nil {
		log.Printf("Error unmarshalling the content: %s", err)
	}
	//log.Printf("Content: %+v", itemInfo)
	itemInfo.SetDate()
	log.Printf("HeadItem: %+v", itemInfo.Items[0])
	log.Printf("HeadItem Created: %s", itemInfo.Items[0].GetTime())

	urlObj = item.GenerateMetaLink(*itemId)
	log.Printf("url: %+v", urlObj)

	resp, err = http.Get(urlObj.String())
	if err != nil {
		log.Printf("Error gettin the meta content: %s", err)
		return
	}
	defer resp.Body.Close()
	content, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading the meta content: %s", err)
		return
	}
	// log.Printf("Content:\n%s", string(content))
	itemMeta := new(item.Meta)
	err = json.Unmarshal(content, itemMeta)
	if err != nil {
		log.Printf("Error unmarshalingl the meta content: %s", err)
	}

	log.Printf("MetaData: %+v", itemMeta)
	log.Printf("Tag: %+v", itemMeta.Tags[0])
	log.Printf("Comment: %+v", itemMeta.Comments[0])

	item.Login(*userName, *passWord)
}
