package item

import "time"

type Info struct {
	AtEnd     bool    `json:"atEnd"`
	AtStart   bool    `json:"atStart"`
	Cache     string  `json:"cache"`
	Error     string  `json:"error"`
	QC        int     `json:"qc"`
	RT        int     `json:"rt"`
	TS        int64   `json:"ts"`
	Items     []*Item `json:"items"`
	timeStamp time.Time
	Meta      *Meta `json:"-"`
}

func (ii *Info) SetDate() {
	ii.timeStamp = time.Unix(ii.TS, 0)
	for _, i := range ii.Items {
		i.SetDate()
	}
}
