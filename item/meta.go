package item

import (
	"fmt"
	"log"
	"net/url"
	"time"
)

type Meta struct {
	Cache     string `json:"cache"`
	QC        int    `json:"qc"`
	RT        int    `json:"rt"`
	TS        int64  `json:"ts"`
	timeStamp time.Time
	Comments  []*Comment `json:"comments"`
	Tags      []*Tag     `json:"tags"`
}

func GenerateMetaLink(id int) *url.URL {
	urlString := fmt.Sprintf("http://pr0gramm.com/api/items/info?itemId=%d", id)
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Can't parse URL: %s", err)
		return nil
	}
	return urlObj
}
