package item

import "time"

type MetaUser struct {
	Admin       int    `json:"admin"`
	Banned      int    `json:"banned"`
	BannedUntil int64  `json:"bannedUntil"`
	Id          int    `json:"id"`
	Mark        int    `json:"mark"`
	Name        string `json:"name"`
	Registered  int64  `json:"registered"`
	Score       int    `json:"score"`
}

type Badge struct {
	Created     int64  `json:"created"`
	Description string `json:"description"`
	Image       string `json:"image"`
	Link        string `json:"link"`
}

type Upload struct {
	Id        int    `json:"id"`
	Thumbnail string `json:"thumb"`
}

type User struct {
	Badges         []*Badge   `json:"badges"`
	Cache          string     `json:"cache"`
	CommentCount   int        `json:"commentCount"`
	Comments       []*Comment `json:"comments"`
	FollowCount    int        `json:"followCount"`
	Following      bool       `json:"following"`
	LikeCount      int        `json:"likeCount"`
	Likes          []string   `json:"likes"`
	LikesArePublic bool       `json:"likesArePublic"`
	QC             int        `json:"qc"`
	RT             int        `json:"rt"`
	TagCount       int        `json:"tagCount"`
	UploadCount    int        `json:"uploadCount"`
	TS             int64      `json:"ts"`
	timeStamp      time.Time
	Uploads        []*Upload `json:"uplooads"`
	User           MetaUser  `json:"user"`
}
