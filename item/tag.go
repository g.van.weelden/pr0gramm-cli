package item

type Tag struct {
	Confidence float64 `json:"confidence"`
	Tag        string  `json:"tag"`
	Id         int     `json:"id"`
}
