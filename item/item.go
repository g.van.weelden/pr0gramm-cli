package item

import (
	"fmt"
	"log"
	"net/url"
	"time"
)

type Item struct {
	Created   int64 `json:"created"`
	createdTS time.Time
	Down      int    `json:"down"`
	Up        int    `json:"up"`
	Flags     int    `json:"flags"`
	Image     string `json:"image"`
	Mark      int    `json:"mark"`
	Promoted  int    `json:"promoted"`
	Source    string `json:"source"`
	Fullsize  string `json:"fullsize"`
	Thumbnail string `json:"thumb"`
	User      string `json:"user"`
	ID        int    `json:"id"`
	Meta      *Meta  `json:""`
}

func (i *Item) LoadMeta() {}

func (i *Item) SetDate() {
	i.createdTS = time.Unix(i.Created, 0)
}

func (i *Item) GetTime() string {
	return i.createdTS.Format("Mon Jan 2 15:04:05 -0700 MST 2006")
}

func GenerateLink(id int) *url.URL {
	urlString := fmt.Sprintf("http://pr0gramm.com/api/items/get?id=%d&flags=%d", id, SFW+NSFW)
	urlObj, err := url.Parse(urlString)
	if err != nil {
		log.Printf("Can't parse URL: %s", err)
		return nil
	}
	return urlObj
}
