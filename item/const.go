package item

const (
	SCHWUCHTEL           = 0
	NEUSCHWUCHTEL        = 1
	ALTSCHWUCHTEL        = 2
	ADMIN                = 3
	GESPERRT             = 4
	MODERATOR            = 5
	FLIESENTISCHBESITZER = 6
	LEBENDELEGENDE       = 7
	PR0WICHTLER          = 8
	EDLERSPENDER         = 9
)

var Mark = map[int]string{
	SCHWUCHTEL:           "Schwuchtel",
	NEUSCHWUCHTEL:        "Neuschwuchtel",
	ALTSCHWUCHTEL:        "Altschwuchtel",
	ADMIN:                "Admin",
	GESPERRT:             "Gesperrt",
	MODERATOR:            "Moderator",
	FLIESENTISCHBESITZER: "Fliesentischbesitzer",
	LEBENDELEGENDE:       "Lebende Legende",
	PR0WICHTLER:          "pr0wichtler",
	EDLERSPENDER:         "Edler Spender",
}

const (
	SFW  = 1
	NSFW = 2
	NSFL = 4
)
