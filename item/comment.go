package item

import "time"

type Comment struct {
	Confidence float64 `json:"confidence"`
	Content    string  `json:"content"`
	Created    int64   `json:"created"`
	createsTS  time.Time
	Down       int    `json:"down"`
	Up         int    `json:"up"`
	Mark       int    `json:"mark"`
	Name       string `json:"name"`
	Parent     int    `json:"parent"`
	parent     *Comment
	childs     []*Comment
}
