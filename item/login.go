package item

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

func Login(user string, password string) {
	resp, err := http.PostForm("http://pr0gramm.com/api/user/login",
		url.Values{"name": {user}, "password": {password}})

	if err != nil {
		log.Printf("Error by login: %s", err)
		return
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading the login response: %s", err)
		return
	}
	log.Printf("Login: %s", string(content))
}
